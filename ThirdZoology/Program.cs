﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;

namespace ZoologyThird
{
    class Program
    {
        static void Main(string[] args)
        {
            var animals = new List<Animal>()
            {
                new BlackBear("Quebec, Canada", 30),
                new BlackBear("Värmland, Sweden", 28),
                new Duck("Akerselva, Norway", 8),
                new Herbivore("Hamar, Norway", 12),
                new Carnivore("Hamar, Norway", 12),
                new Duck("Toronto, Canada", 7)
            };

            PrintAnimalList(animals);

            Console.WriteLine();

            #region Search for animals by location
            Console.WriteLine("Search for animals by location: ");
            string search = Console.ReadLine();

            List<Animal> filteredLocation = FromListGetAll(animals, search, (el, str) => 
            {
                str = str.ToLower();

                if (el.Location.ToLower().Contains(str) || el.Location.ToLower() == str)
                {
                    return true;
                }

                return false;
            });
            #endregion

            PrintAnimalList(filteredLocation);

            Console.WriteLine();

            #region Search for animals by age
            Console.WriteLine("Search for animals by age: ");
            search = Console.ReadLine();

            List<Animal> filteredAge = FromListGetAll(animals, search, (el, str) =>
            {
                if (el.Age.ToString().ToLower() == str)
                {
                    return true;
                }

                return false;
            });
            #endregion

            PrintAnimalList(filteredAge);

            Console.WriteLine();

            #region Search by type
            Console.WriteLine("Search for animals by type: ");
            search = Console.ReadLine();
                
            List<Animal> filteredType = FromListGetAll(animals, search, (el, str) =>
            {
                if (el.GetType().Name.ToLower().Contains(str.ToLower()) || el.GetType().Name.ToLower() == str.ToLower())
                {
                    return true;
                }

                return false;
            });
            #endregion

            PrintAnimalList(filteredType);
        }

        #region Local methods
        private static void PrintAnimalList(List<Animal> animals)
        {
            foreach (var item in animals)
            {
                Console.WriteLine(item);
            }
        }

        private static List<Animal> FromListGetAll(List<Animal> list, string search, Filter filter)
        {
            return (from element in list
                   where filter(element, search)
                   select element).ToList();
        }

        private delegate bool Filter(Animal animal, string search);
        #endregion
    }
}

